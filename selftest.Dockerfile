#+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-#
#       |S|c|i|e|n|c|e| |B|o|x|        #
#+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-+-#

# Dockerfile for selftest image

# Dockerfile to run basic tests for the docker-compose based deployment (https://github.com/cernbox/uboxed)
# NOTE: The selftest container needs access to the Docker daemon socket on the host 
#       to execute tests on other containers (e.g., JupyterHub, CVMFS, etc...).
#       This can be achieved by mounting the socket as a volume: `/var/run/docker.sock:/var/run/docker.sock:rw`


# Note: only tag 'latest' is supported

# Build and push to Docker registry with:
#   docker build -t gitlab-registry.cern.ch/sciencebox/docker-images/selftest -f selftest.Dockerfile .
#   docker login gitlab-registry.cern.ch
#   docker push gitlab-registry.cern.ch/sciencebox/docker-images/selftest


FROM gitlab-registry.cern.ch/sciencebox/docker-images/parent-images/base:v0

MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>


# ----- Install the required software ----- #
RUN yum -y install \
      nmap \
      wget \
      yum-utils && \
    yum clean all && \
    rm -rf /var/cache/yum

# ----- Install docker ----- #
RUN yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
RUN yum -y install \
      docker-ce && \
    yum clean all && \
    rm -rf /var/cache/yum

# ----- Copy the tests ----- #
ADD ./selftest.d /root/selftest.d

# ----- Sleep forever when started ----- #
# NOTE: The preparation and execution of tests is demanded to a separate script
CMD ["/bin/bash"]
